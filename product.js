
const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  }
];

let div = document.getElementById('root');
let list = document.createElement('ul');
div.append(list);
// let div = document.createElement('div');
// document.append(div);
// div.setAttribute('id', 'root');

books.forEach(item => {

  try {
    if (!item.hasOwnProperty('author')) {
      throw new SyntaxError(item + " not have a author");
    }
    if (!item.hasOwnProperty('name')) {
      throw new SyntaxError(item + " not have a name");
    }
    if (!item.hasOwnProperty('price')) {
      throw new SyntaxError(item + " not have a price");
    }

    let listItem = document.createElement('li');

    list.appendChild(listItem);
    listItem.innerText = `name: ${item.name} author: ${item.author} price: ${item.price}`;
  } catch (err) {
    console.log(err.message);
  }
})

